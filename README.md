# **NodeMCU 2.1.0 (Enterprise WIFI)** #

This is a fork from [nodemcu-firmware](https://github.com/nodemcu/nodemcu-firmware) witch allows you to connect to enterprise wifi.

# Build the firmware 

After clone this repository enter into cloned directory an run following command:

```bash
sudo docker run --rm -ti -v `pwd`:/opt/nodemcu-firmware -e ENTERPRISE_USERNAME=username -e ENTERPRISE_PASSWORD=password -e ENTERPRISE_DOMAIN=domain marcelstoer/nodemcu-build
```
Replace *username*, *password* and *domain* with your values.

To connect configure only ssid without password

```lua
-- connect to enterprise WiFi access point
wifi.setmode(wifi.STATION)
staConf = {
    ssid = "ENTERPRISE_WIFI"
}
wifi.sta.config(staConf)
wifi.sta.connect()

tmr.alarm(1,1000, 1, function()
    if wifi.sta.getip()==nil then
        print("Waiting for IP address")
    else
        print("Your IP address is " .. wifi.sta.getip())
        tmr.stop(1)
    end
end)
```

# Warning

Length of *username@domain* to be less than 23 symbols
